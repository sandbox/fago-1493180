<?php
/**
 * @file
 * picasa_example_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function picasa_example_views_views_api() {
  return array("version" => "3.0");
}
