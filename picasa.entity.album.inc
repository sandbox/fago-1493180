<?php

/**
 * @file
 * Picasa module, Entity related classes.
 */

/**
 * Entity controller for picasa albums.
 */
class PicasaAlbumEntityController extends EntityAPIController {

  /**
   * @var Zend_Gdata_Photos
   */
  protected $service;

  public function __construct($entity_type) {
    parent::__construct($entity_type);

    $this->client = picasa_client();
    $this->service = new Zend_Gdata_Photos($this->client);
  }

  /**
   * Retrieves albums.
   *
   * @return The results in a Traversable object.
   */
  public function query($ids, $conditions, $revision_id = FALSE) {
    if ($ids === FALSE) {
      return $this->getAll();
    }

    $entities = array();
    foreach ($ids as $id) {
      $entity = new $this->entityInfo['entity class']($this->getEntry($id));
      $entities[$entity->id] = $entity;
    }
    return $entities;
  }

  /**
   * Get all albums of our configured user.
   */
  protected function getAll() {
    $entities = array();
    $userFeed = $this->service->getUserFeed(picasa_user_account());
    foreach ($userFeed as $userEntry) {
      $entity = new $this->entityInfo['entity class']($userEntry);
      $entities[$entity->id] = $entity;
    }
    return $entities;
  }

  /**
   * Gets a single album entry by ID.
   */
  protected function getEntry($id) {
    $query = new Zend_Gdata_Photos_AlbumQuery();
    $query->setAlbumId($id);
    $query->setType("entry");
    return $this->service->getAlbumEntry($query);
  }

  public function create(array $values = array()) {
    throw new Exception("Creation is not supported.");
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    throw new Exception("Saving is not supported.");
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    throw new Exception("Deletion is not supported.");
  }
}

/**
 * The picasa album entity.
 */
class PicasaAlbumEntity extends Entity {

  protected $entityType = 'picasa_album';

  /**
   * @var Zend_Gdata_Photos_AlbumEntry
   */
  protected $entry;

  public function __construct(Zend_Gdata_Photos_AlbumEntry $entry = NULL) {
    $this->entry = isset($entry) ? $entry : new Zend_Gdata_Photos_AlbumEntry();

    foreach ($this->properties() as $name => $info) {
      if (empty($info['getter callback'])) {
        $picasa_name = isset($info['picasa name']) ? $info['picasa name'] : 'gphoto' . $name;
        $this->$name = call_user_func(array($this->entry, 'get' . $picasa_name))->getText();
      }
    }
  }

  /**
   * Applies any possible changes of the entity to the Gdata entry.
   */
  public function updateEntry() {
    foreach ($this->properties() as $name => $info) {
      if (empty($info['getter callback'])) {
        $picasa_name = isset($info['picasa name']) ? $info['picasa name'] : 'gphoto' . $name;
        call_user_func(array($this->entry, 'set' . $picasa_name), $this->$name);
      }
    }
  }

  /**
   * Returns the remote URL of this resource.
   */
  public function remoteURL() {
    return $this->entry->getId()->getText();
  }

  /**
   * Returns info about the properties of this entity, as declared in hook_entity_property_info().
   *
   * @return array
   *   The properties defined for this entity.
   *
   * @see picasa_entity_property_info()
   */
  public function properties() {
    $info = entity_get_property_info($this->entityType());
    return $info['properties'];
  }

  /**
   * Returns the Zend Gdata API entry object of this entity.
   *
   * @return Zend_Gdata_Photos_AlbumEntry
   */
  public function entry() {
    $this->updateEntry();
    return $this->entry;
  }
}
