<?php

/**
 * @file
 * Picasa module.
 */

/**
 * Implements hook_entity_info().
 */
function picasa_entity_info() {
  $items['picasa_album'] = array(
    'label' => t('Picasa album'),
    'plural label' => t('Picasa albums'),
    'description' => t('Albums retrieved from google picasa.'),
    'entity class' => 'PicasaAlbumEntity',
    'controller class' => 'PicasaAlbumEntityController',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'name',
    ),
    'module' => 'picasa',
  );
  $items['picasa_photo'] = array(
    'label' => t('Picasa photo'),
    'plural label' => t('Picasa photos'),
    'description' => t('Photos retrieved from google picasa.'),
    'entity class' => 'PicasaPhotoEntity',
    'controller class' => 'PicasaPhotoEntityController',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'name',
    ),
    'module' => 'picasa',
  );
  return $items;
}

/**
 * Implements hook_entity_property_info().
 */
function picasa_entity_property_info() {
  // We just define properties as the webservice provides. For a convenient
  // mapping to the picasa propery names we denote the picasa property names
  // here under the 'picasa name' key. If no 'picasa name' is specified we just
  // default to "gphoto{propertyName}".
  $info['picasa_album']['properties'] = array(
    'name' => array(
      'label' => t('Name'),
    ),
    'numPhotos' => array(
      'label' => t('Number of Photos'),
      'type' => 'integer',
    ),
    'commentCount' => array(
      'label' => t('Count of comments'),
      'type' => 'integer',
    ),
    'id' => array(
      'label' => t('ID'),
      'type' => 'integer',
    ),
    'timestamp' => array(
      'label' => t('Timestamp'),
      'type' => 'date',
    ),
    'user' => array(
      'label' => t('User'),
    ),
    'remoteURL' => array(
      'label' => t('The remote URL of the resource of this entity.'),
      'type' => 'url',
      'getter callback' => 'entity_property_getter_method',
    ),
  );
  $info['picasa_photo']['properties'] = array(
    'id' => array(
      'label' => t('ID'),
      // Make use of the full URL as ID as the gphoto ID is not unique (it's
      // only unique inside an album).
      'picasa name' => 'id',
    ),
    // Refer to the connected album. As the album is defined as entity type we
    // can just use that as data type. The albumId property holds the gphoto
    // album ID what we use as album entity ID, so the system will be able to
    // load the referenced album for us. Else, we would have to implement a
    // 'getter callback' that figures out the right ID or entity object.
    // Thus, the album can be loaded via this entity reference, e.g. by the
    // metadata wrappers and code relying upon it like Entity tokens, Rules or
    // the Entity Views fields.
    'album' => array(
      'label' => t('Album'),
      'type' => 'picasa_album',
      'picasa name' => 'gphotoAlbumId',
    ),
    'commentCount' => array(
      'label' => t('Count of comments'),
      'type' => 'integer',
    ),
    'timestamp' => array(
      'label' => t('Timestamp'),
      'type' => 'date',
    ),
    'width' => array(
      'label' => t('Width'),
      'type' => 'integer',
    ),
    'height' => array(
      'label' => t('Height'),
      'type' => 'integer',
    ),
    'size' => array(
      'label' => t('Size'),
      'type' => 'integer',
    ),
    'remoteURL' => array(
      'label' => t('The remote URL of the resource of this entity.'),
      'type' => 'url',
      'getter callback' => 'entity_property_getter_method',
    ),
  );
  return $info;
}

/**
 * Log into GoogleAPI and return the client.
 */
function picasa_client() {
  // Return the client if it has been loaded already.
  $client = &drupal_static(__FUNCTION__);
  if (!isset($client)) {
    // Initialize the vars for the API call.
    $serviceName = Zend_Gdata_Photos::AUTH_SERVICE_NAME;
    $user = variable_get('picasa_google_user', '');
    $pass = variable_get('picasa_google_pass', '');

    // Attempt to create the client.
    try {
      $client = Zend_Gdata_ClientLogin::getHttpClient($user, $pass, $serviceName);
    }
    catch (Zend_Gdata_App_HttpException $e) {
      watchdog('Picasa', $e->getMessage(), array(), WATCHDOG_ERROR);
      if ($e->getResponse() != NULL) {
        watchdog('Picasa', $e->getResponse()->getBody(), array(), WATCHDOG_ERROR);
      }
    }
    // Not catched exceptions will be catched by controller, logged to the
    // watchdog and re-thrown.
  }
  return $client;
}

/**
 * Gets a the google picasa user to make use of.
 */
function picasa_user_account() {
  return variable_get('picasa_account', 'nuppladev@gmail.com');
}

/**
 * Implements hook_views_api().
 */
function picasa_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'picasa'),
  );
}

/**
 * Implements hook_page_alter() to do some example loads.
 */
function picasa_page_alter() {
  if (arg(0) == 'picasa-album') {
    $r = entity_load_single('picasa_album', 5720117435286591681);
    dpm($r, 'Single album');

    $r = entity_load('picasa_album');
    dpm($r, 'All albums');
  }
  if (arg(0) == 'picasa-photo') {
    // Photos.
    $r = entity_load_single('picasa_photo', 'https://picasaweb.google.com/data/entry/api/user/101657528803489520878/albumid/5720117435286591681/photoid/5720117843868754498');
    dpm($r, 'Single photo');

    $wrapper = entity_metadata_wrapper('picasa_photo', $r);
    debug($wrapper->remoteURL->value());

    $r = entity_load('picasa_photo');
    dpm($r, 'All photos');
  }
}
