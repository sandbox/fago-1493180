<?php

/**
 * @file
 * Picasa module, Entity related classes.
 */

/**
 * Entity controller for picasa photos.
 */
class PicasaPhotoEntityController extends EntityAPIController {

  /**
   * @var Zend_Gdata_Photos
   */
  protected $service;

  public function __construct($entity_type) {
    parent::__construct($entity_type);

    $this->client = picasa_client();
    $this->service = new Zend_Gdata_Photos($this->client);
  }

  /**
   * Retrieves albums.
   *
   * @return The results in a Traversable object.
   */
  public function query($ids, $conditions, $revision_id = FALSE) {
    if ($ids === FALSE) {
      return $this->getAll();
    }

    $entities = array();
    foreach ($ids as $id) {
      $entity = new $this->entityInfo['entity class']($this->getEntry($id));
      $entities[$entity->id] = $entity;
    }
    return $entities;
  }

  /**
   * Gets a single photo entry by ID.
   */
  protected function getEntry($id) {
    return $this->service->getPhotoEntry($id);
  }

  /**
   * Get all photos of our configured user.
   */
  protected function getAll() {
    $query = new Zend_Gdata_Photos_UserQuery();
    $query->setUser(picasa_user_account());
    $query->setKind('photo');
    $feed = $this->service->getUserFeed(null, $query);

    $entities = array();
    foreach ($feed as $entry) {
      $entity = new $this->entityInfo['entity class']($entry);
      $entities[$entity->id] = $entity;
    }
    return $entities;
  }

  public function create(array $values = array()) {
    // @todo: Test and fix.
    $entity = new $this->entityInfo['entity class']();
    foreach ($values as $name => $value) {
      $entity->$name = $value;
    }
    $entity->is_new = TRUE;
    return $entity;
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // @todo: Implement creating. Test and fix.
    $entity->entry()->save();
    $this->invoke($entity->is_new ? 'insert' : 'update', $entity);
    $this->resetCache($entity->id);
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    // @todo: Test and fix.
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }
    foreach ($entities as $entity) {
      $entity->entry->delete();
      $this->invoke('delete', $entity);
    }
    $this->resetCache($ids);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    if ($media = $entity->entry()->getMediaGroup()->getContent()) {
      // Just show the image.
      $content['photo'] = array(
        '#theme' => 'image',
        '#path' => $media[0]->getUrl(),
      );
    }
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

/**
 * The picasa photo entity.
 */
class PicasaPhotoEntity extends Entity {

  protected $entityType = 'picasa_photo';

  /**
   * @var Zend_Gdata_Photos_PhotoEntry
   */
  protected $entry;

  public function __construct(Zend_Gdata_Photos_PhotoEntry $entry = NULL) {
    $this->entry = isset($entry) ? $entry : new Zend_Gdata_Photos_PhotoEntry();

    foreach ($this->properties() as $name => $info) {
      if (empty($info['getter callback'])) {
        $picasa_name = isset($info['picasa name']) ? $info['picasa name'] : 'gphoto' . $name;
        $this->$name = call_user_func(array($this->entry, 'get' . $picasa_name))->getText();
      }
    }
  }

  /**
   * Applies any possible changes of the entity to the Gdata entry.
   */
  public function updateEntry() {
    foreach ($this->properties() as $name => $info) {
      if (empty($info['getter callback'])) {
        $picasa_name = isset($info['picasa name']) ? $info['picasa name'] : 'gphoto' . $name;
        call_user_func(array($this->entry, 'set' . $picasa_name), $this->$name);
      }
    }
  }

  /**
   * Returns the remote URL of this resource.
   */
  public function remoteURL() {
    return $this->entry->getId()->getText();
  }

  /**
   * Returns info about the properties of this entity, as declared in hook_entity_property_info().
   *
   * @return array
   *   The properties defined for this entity.
   *
   * @see picasa_entity_property_info()
   */
  public function properties() {
    $info = entity_get_property_info($this->entityType());
    return $info['properties'];
  }

  /**
   * Returns the Zend Gdata API entry object of this entity.
   *
   * @return Zend_Gdata_Photos_AlbumEntry
   */
  public function entry() {
    $this->updateEntry();
    return $this->entry;
  }
}
